import React from 'react';

/* 
    Reading List:
        >>https://dev.to/ruppysuppy/redux-vs-context-api-when-to-use-them-4k3p
        >>https://www.loginradius.com/blog/engineering/react-context-api/
        >>https://dev.to/anuraggharat/how-to-use-react-context-api-5a6a --- MUST READ
*/
//You first create a context
const UserContext = React.createContext();

//Then you need to create a provider
export const UserProvider = UserContext.Provider;
export default UserContext;
