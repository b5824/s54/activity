/* 
Activity s53
>> Create Error.js under pages folder
>> Create a route which will be accessed when a user enters an undefined route and display the Error page.
>> Refactor the Banner component to be useable for the Error page and the Home page.
>> Render the Error page in App.js
>> Send your outputs in Hangouts and link it in Boodle
*/

/* 
Activity s54
  >> Consume the user state from User Context in Register page
  >> If the user goes to /register route (Registration page) when a user is already logged in, redirect the user to the /courses route (Courses page).
  >> Send your outputs in Hangouts and link it in Boodle
*/
import Banner from '../components/Banner';

export default function Error() {
  //   const data = {
  //     error: '404',
  //     message: 'Page Not Found',
  //     route: '/error',
  //     redirection: '/',
  //   };

  return <Banner status='404' />;
}
